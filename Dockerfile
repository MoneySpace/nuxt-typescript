FROM node:10.16.0
WORKDIR /app
# copy the app, note .dockerignore
ADD package.json .
RUN yarn install

# expose 5000 on container
EXPOSE 5000
# set app serving to permissive / assigned
ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=5000
ENV API_URL=http://172.104.207.207/api
ADD . .
RUN yarn build

# start the app
CMD [ "yarn", "start" ]
