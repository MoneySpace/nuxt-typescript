import type { Context } from '@nuxt/types'
import type { GetterTree, ActionTree, MutationTree } from 'vuex'

export interface RootState {
  currentlanguage: any,
  pages: {
      titlefavicon: any,
      header: any,
      footer: any,
      home: any,
      about: any,
      contact: any,
      prohibitedbusinesses: any,
      pricingplans: any,
      integrations: any,
      paymentlinks: any,
      checkoutdemo: any
  }
}

export const state = (): RootState => ({
  currentlanguage: {},
  pages: {
      titlefavicon: {},
      header: {},
      footer: {},
      home: {},
      about: {},
      contact: {},
      prohibitedbusinesses: {},
      pricingplans: {},
      integrations: {},
      paymentlinks: {},
      checkoutdemo: {}
  }
})

export const getters: GetterTree<RootState, RootState> = {
  getCurrentlanguageData: (state): any => state.currentlanguage,
  getTitlefaviconData: (state):any => state.pages.titlefavicon,
  getHeaderData: (state):any => state.pages.header,
  getFooterData: (state):any => state.pages.footer,
  getHomeData: (state):any => state.pages.home,
  getAboutData: (state):any => state.pages.about,
  getContactData: (state):any => state.pages.contact,
  getProhibitedbusinessesData: (state):any => state.pages.prohibitedbusinesses,
  getPricingplansData: (state):any => state.pages.pricingplans,
  getIntegrationsData: (state):any => state.pages.integrations,
  getPaymentlinksData: (state):any => state.pages.paymentlinks,
  getCheckoutdemoData: (state):any => state.pages.checkoutdemo
  
  
}

export const MutationType = {
  SET_CURRENTLANGUAGE: "setCurrentLanguage",
  SET_FOOTER: "setFooter",
  SET_TITLEFAVICON: "setTitleFavIcon",
  SET_HEADER: "setHeader",
  SET_HOME: "setHome",
  SET_ABOUT: "setAbout",
  SET_CONTACT: "setContact",
  SET_PROHIBITEDBUSINESSES : "setProhibitedBusinesses",
  SET_PRICINGPLANS: "setPricingPlans",
  SET_INTEGRATIONS: "setIntegrations",
  SET_PAYMENTLINKS: "setPaymentLinks",
  SET_CHECKOUTDEMO: "setCheckOutDemo"
}

export const mutations: MutationTree<RootState> = {
  [MutationType.SET_CURRENTLANGUAGE]: (state, payload: any) => { state.currentlanguage = payload },
  [MutationType.SET_FOOTER]: (state, payload: any) => { state.pages.footer = payload },
  [MutationType.SET_TITLEFAVICON]: (state, payload: any) => { state.pages.titlefavicon = payload },
  [MutationType.SET_HEADER]: (state, payload: any) => { state.pages.header = payload },
  [MutationType.SET_HOME]: (state, payload: any) => { state.pages.home = payload },
  [MutationType.SET_ABOUT]: (state, payload: any) => { state.pages.about = payload },
  [MutationType.SET_CONTACT]: (state, payload: any) => { state.pages.contact = payload },
  [MutationType.SET_PROHIBITEDBUSINESSES]: (state, payload: any) => { state.pages.prohibitedbusinesses = payload },
  [MutationType.SET_PRICINGPLANS]: (state, payload: any) => { state.pages.pricingplans = payload },
  [MutationType.SET_INTEGRATIONS]: (state, payload: any) => { state.pages.integrations = payload },
  [MutationType.SET_PAYMENTLINKS]: (state, payload: any) => { state.pages.paymentlinks = payload },
  [MutationType.SET_CHECKOUTDEMO]: (state, payload: any) => { state.pages.checkoutdemo = payload }
}

export const actionType = {
  SET_LANGUAGE: 'setLanguage'
}

export const actions: ActionTree<RootState, RootState> = {
  async nuxtServerInit ({ commit,dispatch }, _context: Context) {
    const Titlefavicon = await _context.app.$strapi.$titlefavicon.find(); 
    const Header = await _context.app.$strapi.$header.find();
    const Footer = await _context.app.$strapi.$footer.find();
    const Home = await _context.app.$strapi.$home.find();
    const About = await _context.app.$strapi.$about.find();
    const Contact = await _context.app.$strapi.$contact.find();
    const ProhibitedBussinesses = await _context.app.$strapi.$prohibitedbusinesses.find();
    const PricingPlans =  await _context.app.$strapi.$pricingplans.find();
    const Integrations = await _context.app.$strapi.$integrations.find();
    const PaymentLinks = await _context.app.$strapi.$paymentlinks.find();
    const CheckoutDemo = await _context.app.$strapi.$checkoutdemo.find();
    
    for (let i=0; i < Header.language_button_list.length; i++) {
      if (Header.language_button_list[i].isDefaultActive) {
        dispatch('setLanguage',Header.language_button_list[i]);  
        break;
      }
    }

    commit(MutationType.SET_TITLEFAVICON ,Titlefavicon);
    commit(MutationType.SET_HEADER ,Header);
    commit(MutationType.SET_FOOTER ,Footer);
    commit(MutationType.SET_HOME ,Home);
    commit(MutationType.SET_ABOUT ,About);
    commit(MutationType.SET_CONTACT ,Contact);
    commit(MutationType.SET_PROHIBITEDBUSINESSES ,ProhibitedBussinesses);
    commit(MutationType.SET_PRICINGPLANS ,PricingPlans);
    commit(MutationType.SET_INTEGRATIONS ,Integrations);
    commit(MutationType.SET_PAYMENTLINKS ,PaymentLinks);
    commit(MutationType.SET_CHECKOUTDEMO ,CheckoutDemo);
  },
  [actionType.SET_LANGUAGE]({ commit }, payload: any){
    if(process.browser){
      localStorage.setItem('language', JSON.stringify(payload));
    }
    commit(MutationType.SET_CURRENTLANGUAGE,payload);
  }
}

