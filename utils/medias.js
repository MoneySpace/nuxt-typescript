export function getStrapiMedia(url) {
  url = url+'';  
  // Check if URL is a local path
    if (url.indexOf('/')==0) {
      // Prepend Strapi address
      return `${process.env.API_URL}${url}`;
    }
    // Otherwise return full URL
    return url;
  }