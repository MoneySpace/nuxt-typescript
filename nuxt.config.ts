import type { NuxtConfig } from '@nuxt/types'

const config: NuxtConfig = {
  build: {},
  buildModules: [
    '@nuxt/typescript-build'
  ],
  css: ['~/assets/fonts/stylesheet.css','~/assets/fonts/icomoon/style.css','~/assets/scss/style.scss'],
  env: {},
  head: {
    title: 'Money Space .',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'A boilerplate to start a Nuxt+TS project quickly' }
    ],
    link: [
      { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css' }
      
    ],
    script:[
      {src:'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'},
      {src:'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js'},
      {src:'https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js'},
      {src: "script.js"},
      {src: "modal.js"}
    ]
  },
  loading: { color: '#0c64c1' },
  modules: ['@nuxtjs/strapi'],
  strapi: {
    url: process.env.API_URL || "http://localhost:1337",
    entities: [
      'titlefavicon',
      'header',
      'footer',
      'home',
      'about',
      'contact',
      'prohibitedbusinesses',
      'pricingplans',
      'integrations',
      'paymentlinks',
      'checkoutdemo'
    ],
  },
  plugins: [
    '~/plugins/truncate'
  ]
}

export default config
